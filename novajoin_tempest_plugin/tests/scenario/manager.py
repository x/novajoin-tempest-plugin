# Copyright 2012 OpenStack Foundation
# Copyright 2013 IBM Corp.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import os
from oslo_log import log

from tempest.common import image as common_image
from tempest.common import waiters
from tempest import config
from tempest.lib.common.utils import data_utils
import tempest.scenario.manager

CONF = config.CONF

LOG = log.getLogger(__name__)


class ScenarioTest(tempest.scenario.manager.ScenarioTest):
    """Base class for scenario tests. Uses tempest own clients. """

    credentials = ['primary', 'admin']

    # ## Test functions library
    #
    # The create_[resource] functions only return body and discard the
    # resp part which is not used in scenario tests

    def create_server(self, name=None, image_id=None, flavor=None,
                      net_id=None, key=None, wait_until='ACTIVE',
                      sec_grps=[], metadata={}, **kwargs):
        networks = [{'uuid': net_id}]
        server = self.servers_client.create_server(name=name,
                                                   imageRef=image_id,
                                                   flavorRef=flavor,
                                                   key_name=key,
                                                   security_groups=sec_grps,
                                                   networks=networks,
                                                   metadata=metadata,
                                                   **kwargs)['server']
        server_id = server['id']
        waiters.wait_for_server_status(self.servers_client, server_id,
                                       'ACTIVE')
        return server

    def image_create(self, name, fmt,
                     disk_format=None, properties=None):
        if properties is None:
            properties = {}
        name = data_utils.rand_name('%s-' % name)
        params = {
            'name': name,
            'container_format': fmt,
            'disk_format': disk_format or fmt,
        }
        if CONF.image_feature_enabled.api_v1:
            params['is_public'] = 'False'
            params['properties'] = properties
            params = {'headers': common_image.image_meta_to_headers(**params)}
        else:
            params['visibility'] = 'private'
            # Additional properties are flattened out in the v2 API.
            params.update(properties)
        body = self.image_client.create_image(**params)
        image = body['image'] if 'image' in body else body
        # self.addCleanup(self.image_client.delete_image, image['id'])
        self.assertEqual("queued", image['status'])

        img_path = CONF.scenario.img_file
        if not os.path.exists(img_path):
            # TODO(kopecmartin): replace LOG.warning for raising
            # InvalidConfiguration exception when this plugin is meant to be
            # used with only Tempest 27.0.0 and higher
            LOG.warning(
                'Starting Tempest 25.0.0 release, CONF.scenario.img_file need '
                'a full path for the image. CONF.scenario.img_dir was '
                'deprecated and is removed in 27.0.0 release. Till Tempest '
                '27.0.0 (excluding), old behavior is maintained and kept '
                'working but starting Tempest 27.0.0, only full path '
                'CONF.scenario.img_file config option is accepted by the '
                'Tempest.')
            img_path = os.path.join(CONF.scenario.img_dir, img_path)

        with open(img_path, 'rb') as image_file:
            if CONF.image_feature_enabled.api_v1:
                self.image_client.update_image(image['id'], data=image_file)
            else:
                self.image_client.store_image_file(image['id'], image_file)
        return image['id']
