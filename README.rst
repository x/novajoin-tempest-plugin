===============================
Tempest Integration of Novajoin
===============================

This project defines a tempest plugin containing tests used to verify the
functionality of a novajoin installation. The plugin will automatically load
these tests into tempest.

Dependencies
------------
The novajoin_tempest_plugin tests the novajoin API, which requires the
existence of an IPA server.

Developers
----------
For more information on novajoin, refer to:
https://opendev.org/x/novajoin

For more information on tempest plugins, refer to:
https://docs.openstack.org/tempest/latest/plugins/index.html

Bugs
----
Please report bugs to: http://bugs.launchpad.net/novajoin
